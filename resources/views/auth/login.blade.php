@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
         <div class="w-8/12 bg-white p-6 rounded-lg shadow-2xl">
            @if (session()->has('status') || $status)
                <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                    {{ session()->has('status') ? session('status') : $status }}
                </div>
            @endif

            <form action="{{ route('login') }}" method="POST">
                @csrf
                
                <div class="mb-4">
                    <label for="email" class="sr-only">Email</label>
                    <input type="email" name="email" id="email" placeholder="Your email" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror" value="{{ old('email') }}" />
                    
                    @error('email')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="mb-4">
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" placeholder="Your password" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password') border-red-500 @enderror" value="" />
                    
                    @error('password')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="mb-4">
                    <div class="flex items-center">
                        <input type="checkbox" name="remember" id="remember" class="mr-2">
                        <label for="remember">Remember Me</label>
                    </div>
                </div>

                <div class="flex">
                    <input type="hidden" name="redirectTo" value="{{ request()->get('url') }}">
                    <button type="submit" class="bg-green-500 text-white px-4 py-3 rounded font-medium w-full mr-1">Sign In</button>
                </div>
                
                <div class="mt-3">
                    <p>
                        You don't have an account?
                        <a class="text-blue-900 hover:underline" href="{{ route('register') }}">Register here</a>.
                    </p>
                </div>
            </form>
         </div>
    </div>
@endsection