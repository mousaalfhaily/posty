@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
         <div class="w-8/12 bg-white p-6 rounded-lg shadow-2xl">
            @auth
                <form action="{{ route('posts') }}" method="POST" class="mb-4">
                    @csrf

                    <div class="mb-4">
                        <label for="body" class="sr-only">Body</label>
                        <textarea name="body" id="body" cols="30" rows="4" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('body') border-red-500 @enderror" placeholder="Post something!"></textarea>

                        @error('body')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div>
                        <button type="submit" class="bg-green-500 text-white px-4 py-2 rounded font-medium">Post</button>
                        <button type="reset" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Clear</button>
                    </div>
                </form>
            @endauth

            @guest
                <div class="bg-blue-600 p-4 rounded-lg mb-6 text-white text-center">
                    <a href="{{ route('register', 'url=posts') }}" class="text-blue-200 hover:underline">Reagister a new account</a> or <a href="{{ route('login', '?url=posts') }}" class="text-blue-200 hover:underline">login</a> to your account to create new posts!
                </div>
            @endguest

            @if (session()->has('unauthorized'))
                <div class="bg-red-600 p-4 rounded-lg mb-6 text-white text-center">
                    {{ session('unauthorized') }}
                </div>
            @endif

            @if (session()->has('likeStatus'))
                <div class="bg-red-600 p-4 rounded-lg mb-6 text-white text-center">
                    {{ session('likeStatus') }}
                </div>
            @endif

            @if ($posts->count())
                @foreach ($posts as $post)
                    <x-post :post="$post" />
                @endforeach

                {{ $posts->links() }}
            @else
                <p>There are no posts!</p>
            @endif
         </div>
    </div>
@endsection