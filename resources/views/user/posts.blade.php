@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
         <div class="w-8/12 bg-white p-6 rounded-lg shadow-2xl">
            <h1 class="text-lg font-bold">{{ $user->name }}'s Posts</h1>
            <p class="mb-1">Posted {{ $posts->count() }} {{ Str::plural('post', $posts->count()) }} and received {{ $user->receivedLikes->count() }} {{ Str::plural('like', $user->receivedLikes->count()) }}</p>

            <hr class="border-gray-300">

            <div class="mt-4">
                @if ($posts->count())
                    @foreach ($posts as $post)
                        <x-post :post="$post" />
                    @endforeach

                    {{ $posts->links() }}
                @else
                    <p>{{ $user->name }} doesn't have any posts!</p>
                @endif
            </div>
         </div>
    </div>
@endsection