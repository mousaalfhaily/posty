@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 mb-6 rounded-lg shadow-2xl">
            <h1 class="text-lg font-bold">My account's Settings</h1>

            <hr class="border-gray-300">

            @if (session()->has('status'))
                <div class=" p-4 rounded-lg my-6 text-white text-center @if (session('status')->type
                    == 'error') bg-red-500
                @else
                    bg-green-500 @endif">
                    {{ session('status')->message }}
                </div>
            @endif

            <div class="mt-4">
                <form action="{{ route('account') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="mb-4">
                        <label for="name" class="sr-only">Name</label>
                        <input type="text" name="name" id="name" placeholder="Your Name"
                            value="{{ auth()->user()->name }}"
                            class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror"
                            value="{{ old('name') }}" />

                        @error('name')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <label for="username" class="sr-only">Username</label>
                        <input type="text" name="username" id="username" placeholder="Your Username"
                            value="{{ auth()->user()->username }}"
                            class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('username') border-red-500 @enderror"
                            value="{{ old('username') }}" />

                        @error('username')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <label for="email" class="sr-only">Email</label>
                        <input type="email" name="email" id="email" placeholder="Your email"
                            value="{{ auth()->user()->email }}"
                            class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror"
                            value="{{ old('email') }}" />

                        @error('email')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <label for="password" class="sr-only">Current Password</label>
                        <input type="password" name="current_password" id="current_password"
                            placeholder="Your current password (Required)"
                            class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('current_password') border-red-500 @enderror" />

                        @error('current_password')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-4">
                        <label for="password" class="sr-only">New Password</label>
                        <input type="password" name="new_password" id="new_password"
                            placeholder="Your new password (Optional)"
                            class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('new_password') border-red-500 @enderror" />

                        @error('new_password')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="my-6">
                        <div class="w-full h-64 flex border border-gray-400 rounded-lg overflow-hidden">
                            @if (auth()->user()->profile_image)
                                <img id="profile_image_preview" width="256" height="256"
                                    src="{{ URL('images/' . auth()->user()->profile_image) }}"
                                    data-placeholder="{{ URL('images/' . auth()->user()->profile_image) }}"
                                    alt="Profile image">
                            @else
                                <img id="profile_image_preview" width="256" height="256"
                                    src="{{ URL('images/common/profile_image_placeholder.jpg') }}"
                                    data-placeholder="{{ URL('images/common/profile_image_placeholder.jpg') }}"
                                    alt="Profile image">
                            @endif

                            <div class="w-full flex justify-center items-center flex-col">
                                <input type="file" class="sr-only" name="profile_image" id="profile_image" />

                                <label for="profile_image"
                                    class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow cursor-pointer h-11">
                                    @if (auth()->user()->profile_image)
                                        Change Profile Image
                                    @else
                                        Choose Profile Image
                                    @endif
                                </label>

                                <label id="reset_profile_image"
                                    class="bg-red-500 hover:bg-red-600 text-white font-semibold py-2 px-4 border border-red-800 rounded shadow cursor-pointer h-11 mt-2 hidden">Reset
                                    Profile Image</label>

                                {{-- TODO: Enable after submetting the form using AJAX --}}
                                {{-- @if (auth()->user()->profile_image)
                                    <label id="remove_profile_image"
                                        class="bg-red-500 hover:bg-red-600 text-white font-semibold py-2 px-4 border border-red-800 rounded shadow cursor-pointer h-11 mt-2">Remove
                                        Profile Image</label>
                                @endif --}}
                            </div>
                        </div>

                        @error('profile_image')
                            <div class="text-red-500 mt-2 text-sm">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="flex">
                        <input type="hidden" name="redirectTo" value="{{ request()->get('url') }}">
                        <button type="submit" class="bg-green-500 text-white px-4 py-3 rounded font-medium w-full mr-1">Save
                            Settings</button>
                        <button type="reset" class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full ml-1">Reset
                            Form</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal"
        class="fixed top-0 left-0 w-screen h-screen flex items-center justify-center bg-blue-500 bg-opacity-50 transform scale-0 transition-transform duration-300">
        <div class="bg-white w-1/1 h-1/1 p-10 rounded-lg">
            <button id="closebutton" type="button" class="focus:outline-none">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </button>

            <div id="profile_image_editor"></div>

            <button class="bg-green-500 text-white px-4 py-3 rounded font-medium w-full mr-1 save-but">Save
            </button>
        </div>
    </div>
@endsection
