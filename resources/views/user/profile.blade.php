@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
         <div class="w-8/12 bg-white p-6 rounded-lg shadow-2xl">
            <h1 class="text-lg font-bold">{{ $user->name }}'s Profile</h1>

            <hr class="border-gray-300">

            <div class="mt-3">
                <ul class="list-disc pl-4">
                    <li><a class="text-blue-900 hover:underline" href="{{ route('user.posts', $user) }}">Posts</a></li>
                </ul>
            </div>
         </div>
    </div>
@endsection