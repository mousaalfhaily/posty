<div class="mb-4">
    <a href="{{ route('user.profile', $post->user) }}" class="font-bold">{{ $post->user->name }}</a>
    <span class="text-gray-600 text-sm">{{ $post->created_at->diffForHumans() }}</span>
    <p class="mb-2">{{ $post->body }}</p>

    <div class="flex items-center">
        @auth
            @if (!$post->isLikedBy(auth()->user()))
                <form action="{{ route('posts.likes', $post) }}" method="POST" class="ml-0 mr-1">
                    @csrf
                    <button type="submit" class="text-blue-500">Like</button>
                </form>
            @else
                <form action="{{ route('posts.likes', $post) }}" method="POST" class="ml-0 mr-1">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="text-blue-500">Unlike</button>
                </form>
            @endif
        @endauth

        <span>{{ $post->likes->count() }} {{ Str::plural('like', $post->likes->count()) }}</span>

        @can('delete', $post)
            <form action="{{ route('posts.destroy', $post) }}" method="POST" class="ml-1">
                @csrf
                @method('DELETE')
                | <button type="submit" class="text-red-500">Delete</button>
            </form>
        @endcan
    </div>
</div>
