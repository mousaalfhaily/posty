import Croppie from 'croppie';

$(function () {
    var resize = new Croppie($('#profile_image_editor')[0], {
        viewport: {
            width: 300,
            height: 300,
        },
        boundary: {
            width: 500,
            height: 500
        },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });

    $('#profile_image').on('change', function (e) {
        var imageType = e.target.files[0].type;
        
        if (!imageType || !imageType.includes('image')) {
            Swal.fire({
                title: 'Error!',
                text: 'The profile image must be a file of type: jpg, png, jpeg.',
                icon: 'error',
            });

            e.preventDefault();
            return;
        }

        var modal = document.getElementById('modal');
        var closebutton = document.getElementById('closebutton');

        closebutton.addEventListener('click', () => {
            modal.classList.remove('scale-100');
            $('#profile_image').val('');
        });

        if (modal) modal.classList.add('scale-100');
        // resize.destroy();

        setTimeout(() => {
            resize.bind({
                url: URL.createObjectURL(e.target.files[0]),
            });

            $('#modal button.save-but').on('click', function () {
                resize.result().then(function (image) {
                    $('#profile_image_preview').attr('src', image);
                    $(this).prev().html('Change Profile Image');
                    $('#reset_profile_image').removeClass('hidden');
                    modal.classList.remove('scale-100');
                });

                // TODO: Enable after submetting the form using AJAX
                // resize.result('blob').then(function (imageBlob) {
                //     console.log(imageBlob);
                // });
            });
        }, 500);
    });

    $('#reset_profile_image').on('click', function () {
        $('#profile_image_preview').attr('src', $('#profile_image_preview').data('placeholder'));
        $('#profile_image').val('');
        $('#profile_image').prev().html('Choose Profile Image');
        $('#reset_profile_image').addClass('hidden');
    });

    // TODO: Enable after submetting the form using AJAX
    // $('#remove_profile_image').on('click', function () {
    //     $('#profile_image_preview').attr('src', $('#profile_image_preview').data('placeholder'));
    //     $('#image_profile_removed').val('');
    //     $('#profile_image').prev().html('Choose Profile Image');
    //     $('#reset_profile_image').addClass('hidden');
    // });
});
