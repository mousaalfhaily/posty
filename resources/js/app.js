import $ from 'jquery';
window.$ = window.jQuery = $;

import Swal from 'sweetalert2';
window.Swal = require('sweetalert2');

require('./bootstrap');
require('./account');

