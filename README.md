[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://opensource.org/licenses/MIT)

## About Posty
-----------------

A users / posts project with authentication and liking system + a dashboard and an account page.

## Contributing
-----------------

Thank you in advanced if you are considering contributing in Posty! To contribute please:

1. Fork the [repo](https://bitbucket.org/mousaalfhaily/posty/).

2. Clone it to your machine and set it up.

3. Branch out from `production` and name it like: `{your_name}{-featrue-name-or-title}`.

4. Once done create a pull request to the `'testing'` branch and put me as the reviewer (Mousa Alfhaily).

5. I will review the code and leave some comments if any then I'll approve it, merge it, test it and continue with it to be on the `production` branch and released.

## Security Vulnerabilities
-----------------

If you discover a security vulnerability within Laravel, please send an e-mail to Mousa Alfhaily via [mousa.alfhaily@gmail.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License
-----------------

Posty is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Installation & Local Setup
-----------------

#### Requirements

To run this project, you will need

- `PHP 7.4` or higher.

- `Laravel 8.3`.

- `Composer`.

- `NodeJS`.


#### In your terminal

1. Clone the project

```bash
git clone https://mousaalfhaily@bitbucket.org/mousaalfhaily/posty.git
```

2. Go to the project directory

```bash
cd posty
```

3. Install dependencies

```bash
composer install
npm install
```

4. Create the `.env` file

```bash
copy .env.example .env
```

5. Generate the project key

```bash
php artisan key:generate
```

6. Create the database empty

```bash
php artisan db:create
```

7. Migrate all database changes

```bash
php artisan migrate
```

8. Run the project

```bash
php artisan serve
```

At this point you should be setup and ready to view the project in your browser localy.
