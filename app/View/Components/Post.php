<?php

namespace App\View\Components;

use App\Models\Post as ModelsPost;
use Illuminate\View\Component;

class Post extends Component
{
    /**
     * The post instance.
     *
     * @var Post
     */
    public $post;

    /**
     * Create the component instance.
     *
     * @param  Post  $post
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.post');
    }
}
