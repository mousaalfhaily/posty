<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Mail\PostLiked;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PostLikeController extends Controller
{
    /**
     * @param Post $post
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Post $post, Request $request)
    {
        if ($post->isLikedBy($request->user())) {
            return back()->with('likeStatus', 'You already liked this post!');
        }

        $post->likes()->create([
            'user_id' => $request->user()->id,
        ]);

        if (!$post->likes()->onlyTrashed()->where('user_id', $request->user()->id)->count()) {
            Mail::to($post->user)->send(new PostLiked($request->user(), $post));
        }

        return back();
    }

    /**
     * @param Post $post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post, Request $request)
    {
        $request->user()->likes()->where('post_id', $post->id)->delete();
        return back();
    }
}
