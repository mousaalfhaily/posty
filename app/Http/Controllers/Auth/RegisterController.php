<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255|unique:App\Models\User',
            'email' => 'required|email|max:255|unique:App\Models\User',
            'password' => 'min:8|max:255|required|confirmed',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => Str::lower($request->username),
            'password' => Hash::make($request->password),
        ]);

        $isAuthenticated = Auth::attempt($request->only('email', 'password'));

        $redirectTo = $request->redirectTo ? $request->redirectTo : ($isAuthenticated ? 'dashboard' : 'login');
        
        return redirect()->route($redirectTo);
    }
}
