<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * @param null $status
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($status = null)
    {
        if (Auth::check()) return view('user.account');

        switch ($status) {
            case 'unauthenticated':
                $status = 'You don\'t have access to that page!';
                break;
            default:
                $status = null; 
        }

        return view('auth.login', ['status' => $status]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $isRemembered = isset($request->remember);

        if (!Auth::attempt($request->only('email', 'password'), $isRemembered))
            return back()->with('status', 'Invalid login credentials');

        $redirectTo = $request->redirectTo ? $request->redirectTo : 'account';
        
        return redirect()->route($redirectTo);
    }
}
