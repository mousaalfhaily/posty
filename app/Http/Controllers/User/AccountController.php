<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class AccountController extends Controller
{
    public function index()
    {
        return view('user.account');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'current_password' => 'required',
            'new_password' => 'nullable|min:8|max:255',
            'profile_image' => 'mimes:jpg,png,jpeg|max:5048'
        ]);

        $user = User::find(auth()->user()->id);

        if ($user && Hash::check($request->current_password, $user->password)) {
            $dataToUpdate = [
                'name' => $request->name,
                'email' => $request->email,
                'username' => $request->username,
            ];
            
            if (!is_null($request->new_password)) $dataToUpdate['password'] = Hash::make($request->new_password);

            if ($request->profile_image) {
                $profileImageName = time() . '-' . $request->name . '.' . $request->profile_image->extension();
                $fileMoved = false;
                
                try {
                    $request->file('profile_image')->move(public_path('images'), $profileImageName);
                    $fileMoved = true;
                } catch (FileException $ex) {
                    return back()->with('status', (object) [
                        'type' => 'error',
                        'message' => 'There was an error with the profile image!',
                    ]);
                }

                if ($fileMoved) $dataToUpdate['profile_image'] = $profileImageName;
            }
            
            $user->fill($dataToUpdate);
            $user->save();
        } else return back()->with('status', (object) [
            'type' => 'error',
            'message' => 'Incorrect password!',
        ]);

        return back()->with('status', (object) [
            'type' => 'success',
            'message' => 'Info updated successfully',
        ]);
    }
}
